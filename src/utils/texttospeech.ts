// returns base64 binary
export const getSpeechFromText = async (
  text: string,
  name: string,
  speed: number
): Promise<string> => {
  // Executes an API request, and returns a Promise.
  // The method name `texttospeech.text.synthesize` comes from the API discovery.
  // @ts-ignore
  const response = await gapi.client.texttospeech.text.synthesize({
    audioConfig: {
      audioEncoding: "MP3",
      speakingRate: speed,
    },
    voice: {
      languageCode: "en-US",
      name,
    },
    input: {
      text,
    },
  });

  return response.result.audioContent;
};

export interface IVoice {
  languageCodes: string[];
  name: string;
  naturalSampleRateHertz: number;
  ssmlGender: "MALE" | "FEMALE";
}

export const getVoiceList = async (): Promise<IVoice[]> => {
  // Executes an API request, and returns a Promise.
  // The method name `texttospeech.voices.list` comes from the API discovery.
  // @ts-ignore
  const response = await gapi.client.texttospeech.voices.list({
    languageCode: "en-US",
  });

  const voices: IVoice[] = response.result.voices.filter(
    ({ languageCodes }: IVoice) => languageCodes.includes("en-US")
  );

  return voices;
};
