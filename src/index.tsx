import "antd/dist/antd.css";
import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import "./index.css";

async function start() {
  // Initializes the client with the API key and the Translate API.
  await gapi.client.init({
    apiKey: process.env.REACT_APP_GOOGLE_API_KEY,
    discoveryDocs: [
      "https://texttospeech.googleapis.com/$discovery/rest?version=v1",
    ],
  });

  ReactDOM.render(
    <React.StrictMode>
      <App />
    </React.StrictMode>,
    document.getElementById("root")
  );
}

// Loads the JavaScript client library and invokes `start` afterwards.
gapi.load("client", start);
