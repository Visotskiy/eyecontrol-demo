import React, { useRef, useCallback, useEffect } from "react";
import { Button } from "antd";
import { CaretRightOutlined, PauseOutlined } from "@ant-design/icons";
import "./style.css";

interface IOwnProps {
  isLoading: boolean;
  isPlaying: boolean;
  disabled: boolean;
  audioContent: string | null;
  onPlay: (event: React.MouseEvent) => void;
  onStop: (event: React.MouseEvent) => void;
}

const Player = ({
  isLoading,
  isPlaying,
  disabled,
  audioContent,
  onPlay,
  onStop,
}: IOwnProps) => {
  const audioRef = useRef<HTMLAudioElement>(null);
  const togglePlay = useCallback(
    (e) => {
      isPlaying ? onStop(e) : onPlay(e);
    },
    [onStop, onPlay, isPlaying]
  );
  const handleOnEnded = useCallback(
    (e) => {
      onStop(e);
      audioRef.current?.load();
    },
    [audioRef, onStop]
  );

  useEffect(() => {
    if (audioRef.current) {
      isPlaying ? audioRef.current.play() : audioRef.current.pause();
    }
  }, [audioRef, isPlaying]);

  return (
    <>
      <Button
        type="primary"
        shape="circle"
        size="large"
        icon={
          isPlaying ? (
            <PauseOutlined style={{ transform: "scale(1.5)" }} />
          ) : (
            <CaretRightOutlined style={{ transform: "scale(1.5)" }} />
          )
        }
        loading={isLoading}
        onClick={togglePlay}
        disabled={disabled}
      />
      {audioContent && !isLoading && (
        <audio ref={audioRef} onEnded={handleOnEnded}>
          <source src={`data:audio/wav;base64,${audioContent}`} />
        </audio>
      )}
    </>
  );
};

export { Player };
