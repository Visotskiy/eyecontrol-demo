import React, { useMemo } from "react";
import { Select } from "antd";

import { IVoice } from "../../utils/texttospeech";

interface IOwnProps {
  voiceList: IVoice[];
  defaultVoice: IVoice | null;
  onChange: (newVoice: string) => void;
}

const { Option, OptGroup } = Select;

const VoiceList = ({ voiceList, defaultVoice, onChange }: IOwnProps) => {
  const maleVoices = useMemo(
    () => voiceList.filter(({ ssmlGender }) => ssmlGender === "MALE"),
    [voiceList]
  );
  const femaleVoices = useMemo(
    () => voiceList.filter(({ ssmlGender }) => ssmlGender === "FEMALE"),
    [voiceList]
  );

  return (
    <Select
      defaultValue={defaultVoice?.name}
      style={{ width: 200 }}
      onChange={onChange}
    >
      <OptGroup label="Male">
        {maleVoices.map(({ name }) => (
          <Option value={name} key={name}>
            {name}
          </Option>
        ))}
      </OptGroup>
      <OptGroup label="Female">
        {femaleVoices.map(({ name }) => (
          <Option value={name} key={name}>
            {name}
          </Option>
        ))}
      </OptGroup>
    </Select>
  );
};

export { VoiceList };
