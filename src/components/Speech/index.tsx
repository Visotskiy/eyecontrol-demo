import React from "react";
import { Input } from "antd";
import "./style.css";

interface IOwnProps {
  value: string;
  error: string | null;
  onChange: (event: React.ChangeEvent<HTMLTextAreaElement>) => void;
  onEnter: () => void;
}

const Speech = ({ value, onChange, onEnter }: IOwnProps) => {
  return (
    <Input.TextArea
      rows={15}
      onChange={onChange}
      onKeyPress={(event) => {
        if (event.key === "Enter" && event.ctrlKey) onEnter();
      }}
      placeholder="Enter text to speech..."
      className="Speech"
    >
      {value}
    </Input.TextArea>
  );
};

export { Speech };
