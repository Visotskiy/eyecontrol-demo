import React from "react";
import { Select } from "antd";

import { ISpeed } from "../App";

interface IOwnProps {
  speedList: ISpeed[];
  defaultSpeed: ISpeed;
  onChange: (newSpeed: number) => void;
}

const { Option } = Select;

const SpeedList = ({ speedList, defaultSpeed, onChange }: IOwnProps) => {
  return (
    <Select
      defaultValue={defaultSpeed.value}
      style={{ width: 200 }}
      onChange={(value) => onChange(+value)}
    >
      {speedList.map(({ name, value }) => (
        <Option value={value} key={name}>
          {name}
        </Option>
      ))}
    </Select>
  );
};

export { SpeedList };
