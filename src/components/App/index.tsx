import React, { useState, useEffect } from "react";
import { Layout, Skeleton, Space, Row, Col } from "antd";

import { Header } from "../Header";
import { VoiceList } from "../VoiceList";
import { Player } from "../Player";
import { SpeedList } from "../SpeedList";
import { Speech } from "../Speech";

import {
  getSpeechFromText,
  getVoiceList,
  IVoice,
} from "../../utils/texttospeech";

import "./style.css";

interface IVoiceData {
  list: IVoice[];
  isLoading: boolean;
  selected: IVoice | null;
}

export interface ISpeed {
  name: string;
  value: number;
}

interface ISpeedData {
  list: ISpeed[];
  selected: ISpeed;
}

export interface ISpeechData {
  value: string;
  error: string | null;
}

interface IPlayerData {
  audioContent: null | string;
  isLoading: boolean;
  isPlaying: boolean;
  shouldFetch: boolean;
}

const initVoiceData = {
  list: [],
  isLoading: true,
  selected: null,
};

const speedList = [
  {
    name: "x0.5",
    value: 0.5,
  },
  {
    name: "x0.75",
    value: 0.75,
  },
  {
    name: "normal",
    value: 1,
  },
  {
    name: "x1.25",
    value: 1.25,
  },
  {
    name: "x1.5",
    value: 1.5,
  },
];

const initSpeedData = {
  list: speedList,
  selected: speedList[2],
};

const initSpeechData = {
  value: "",
  error: null,
};

const initPlayerData = {
  audioContent: null,
  isLoading: false,
  isPlaying: false,
  shouldFetch: true,
};

function App() {
  const [voiceData, setVoiceData] = useState<IVoiceData>(initVoiceData);
  const [speedData, setSpeedData] = useState<ISpeedData>(initSpeedData);
  const [speechData, setSpeechData] = useState<ISpeechData>(initSpeechData);
  const [playerData, setPlayerData] = useState<IPlayerData>(initPlayerData);

  useEffect(() => {
    getVoiceList().then((voiceList) => {
      setVoiceData({
        list: voiceList,
        selected: voiceList[0],
        isLoading: false,
      });
    });
  }, []);

  const onVoiceChange = (voiceName: string) => {
    const voice: any = voiceData.list.find(({ name }) => name === voiceName);
    setVoiceData({ ...voiceData, selected: voice });
    setPlayerData({ ...playerData, isPlaying: false, shouldFetch: true });
  };

  const onSpeedChange = (speedValue: number) => {
    const speed: any = speedData.list.find(({ value }) => value === speedValue);
    setSpeedData({ ...speedData, selected: speed });
    setPlayerData({ ...playerData, isPlaying: false, shouldFetch: true });
  };

  const onSpeechChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    const value = event.target.value;
    setSpeechData({ ...speechData, value });
    setPlayerData({ ...playerData, isPlaying: false, shouldFetch: true });
  };

  const playAudio = async () => {
    if (!playerData.shouldFetch) {
      return setPlayerData({ ...playerData, isPlaying: true });
    }

    setPlayerData({ ...initPlayerData, isLoading: true });

    const audioContent = await getSpeechFromText(
      speechData.value,
      // @ts-ignore
      voiceData.selected.name,
      speedData.selected.value
    );

    return setPlayerData({
      audioContent,
      isLoading: false,
      isPlaying: true,
      shouldFetch: false,
    });
  };

  const stopAudio = () => {
    setPlayerData({ ...playerData, isPlaying: false });
  };

  return (
    <Layout style={{ height: "100vh", background: "#F0F8FF" }}>
      <Header>
        {!voiceData.isLoading ? (
          <Space align="center">
            <Player
              isLoading={playerData.isLoading}
              isPlaying={playerData.isPlaying}
              audioContent={playerData.audioContent}
              onPlay={playAudio}
              onStop={stopAudio}
              disabled={!voiceData.selected || !speechData.value}
            />
            <VoiceList
              voiceList={voiceData.list}
              defaultVoice={voiceData.selected}
              onChange={onVoiceChange}
            />
            <SpeedList
              speedList={speedData.list}
              defaultSpeed={speedData.selected}
              onChange={onSpeedChange}
            />
          </Space>
        ) : (
          <Space align="center">
            <Skeleton.Button
              className="sceletonItem"
              active
              size="large"
              shape="circle"
            />
            <Skeleton.Button
              className="sceletonItem"
              active
              size="large"
              shape="round"
            />
            <Skeleton.Input
              className="sceletonItem"
              style={{ width: "100px" }}
              active
              size="large"
            />
          </Space>
        )}
      </Header>
      <Layout.Content>
        <Row>
          <Col offset={3} span={18}>
            <Speech
              {...speechData}
              onChange={onSpeechChange}
              onEnter={playAudio}
            />
          </Col>
        </Row>
      </Layout.Content>
    </Layout>
  );
}

export default React.memo(App);
