import React from "react";
import { Layout, Row, Col } from "antd";
import logo from "../../assets/logo.jpg";

import "./style.css";

interface IOwnProps {
  children: string | JSX.Element | JSX.Element[];
}

const Header = ({ children }: IOwnProps) => {
  return (
    <Layout.Header className="l-header">
      <Row align="middle" className="l-header__left">
        <Col span={6}>
          <img src={logo} alt="Company Logo" />
        </Col>
        <Col span={12} className="l-header__middle">
          {children}
        </Col>
      </Row>
    </Layout.Header>
  );
};

export { Header };
